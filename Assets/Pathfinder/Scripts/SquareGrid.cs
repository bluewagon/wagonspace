﻿using System.Collections.Generic;

namespace Pathfinder
{
    public class SquareGrid : WeightedGraph<Square>
    {
        // Implementation notes: I made the fields public for convenience,
        // but in a real project you'll probably want to follow standard
        // style and make them private.

        public static readonly Square[] DIRS = new[]
        {
            new Square(1, 0),
            new Square(0, -1),
            new Square(-1, 0),
            new Square(0, 1)
        };

        private readonly int size;

        public List<List<Square>> board; 

        public SquareGrid(int size)
        {
            this.size = size;
            board = new List<List<Square>>();
        }

        public bool InBounds(int x, int y)
        {
            return 0 <= x && x < size
                && 0 <= y && y < size;
        }

        public bool Passable(Square id)
        {
            return id.IsPassible;
        }

        public int Cost(Square a, Square b)
        {
            return b.Cost;
        }

        public IEnumerable<Square> Neighbors(Square id)
        {
            foreach (var dir in DIRS)
            {
                int x =id.x + dir.x;
                int y = id.y + dir.y;
                if (InBounds(x, y))
                {
                    Square next = board[x][y];
                    if (Passable(next))
                    {
                        yield return next;
                    }
                }
                
                
            }
        }
    }
}