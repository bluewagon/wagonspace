﻿using System.Collections.Generic;

namespace Pathfinder
{
    public interface WeightedGraph<T>
    {
        int Cost(T a, T b);
        IEnumerable<T> Neighbors(T id);
    }
}