﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Eppy;
using Pathfinder;

public class PlayerMovement : MonoBehaviour
{
    public float MoveTime = 0.1f;
    public int MovesPerTurn = 5;

    private Rigidbody2D rBody;
    private float inverseMoveTime;

    private bool isMoving;

	// Use this for initialization
	void Start ()
	{
	    rBody = GetComponent<Rigidbody2D>();
        inverseMoveTime = 1f / MoveTime;
	    isMoving = false;
	    int x = (int) transform.position.x;
	    int y = (int) transform.position.y;
        Debug.Log(string.Format("{0},{1}", x, y));
	    BoardManager.SetCharacter(x, y);
	}
	
	// Update is called once per frame
    void Update() 
    {
	    if (Input.GetMouseButtonDown(0))
	    {


	        Vector3 pz = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            RaycastHit2D hit = Physics2D.Raycast(pz, Vector2.zero);
	        if (hit.collider != null)
	        {
	            Debug.Log(hit.collider.tag);
	        }

	        int x = (int) pz.x;
	        int y = (int) pz.y;
	        if (BoardManager.InBounds(x, y) && BoardManager.IsPassable(x, y) && !isMoving)
	        {
	            isMoving = true;
	            Stack<Square> path = BoardManager.GetPath(Tuple.Create((int) transform.position.x, (int) transform.position.y), Tuple.Create(x, y));
                StartCoroutine(MovePath(path));
	            
	        }
	    }
    }

    private IEnumerator MovePath(Stack<Square> path)
    {
        int moves = 0;
        while (path.Count > 0 && moves < MovesPerTurn)
        {
            moves++;
            Square step = path.Pop();
            yield return StartCoroutine(SmoothMovement(new Vector3(step.x, step.y, 0f)));
        }
        isMoving = false;
    }

    //Co-routine for moving units from one space to next, takes a parameter end to specify where to move to.
    private IEnumerator SmoothMovement(Vector3 end)
    {
        //Calculate the remaining distance to move based on the square magnitude of the difference between current position and end parameter. 
        //Square magnitude is used instead of magnitude because it's computationally cheaper.
        float sqrRemainingDistance = (transform.position - end).sqrMagnitude;

        //While that distance is greater than a very small amount (Epsilon, almost zero):
        while (sqrRemainingDistance > float.Epsilon)
        {
            //Find a new position proportionally closer to the end, based on the moveTime
            Vector3 newPostion = Vector3.MoveTowards(rBody.position, end, inverseMoveTime * Time.deltaTime);

            //Call MovePosition on attached Rigidbody2D and move it to the calculated position.
            rBody.MovePosition(newPostion);

            //Recalculate the remaining distance after moving.
            sqrRemainingDistance = (transform.position - end).sqrMagnitude;

            //Return and loop until sqrRemainingDistance is close enough to zero to end the function
            yield return null;
        }
    }
}
