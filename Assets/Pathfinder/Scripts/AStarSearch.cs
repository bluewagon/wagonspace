﻿using System;
using System.Collections.Generic;

namespace Pathfinder
{
    public class AStarSearch
    {
        public Dictionary<Square, Square> cameFrom
            = new Dictionary<Square, Square>();
        public Dictionary<Square, int> costSoFar
            = new Dictionary<Square, int>();

        public PriorityQueue<Square> frontier;

        private Square start;
        private Square goal;

        // Note: a generic version of A* would abstract over Square and
        // also Heuristic
        static public int Heuristic(Square a, Square b)
        {
            return 2 * (Math.Abs(a.x - b.x) + Math.Abs(a.y - b.y));
        }

        public AStarSearch(WeightedGraph<Square> graph, Square start_, Square goal_)
        {
            this.start = start_;
            this.goal = goal_;

            frontier = new PriorityQueue<Square>();
            frontier.Enqueue(start, 0);

            cameFrom.Add(start, start);
            costSoFar.Add(start, 0);

            while (frontier.Count > 0)
            {
                var current = frontier.Dequeue();

                if (current.Equals(goal))
                {
                    break;
                }

                foreach (var next in graph.Neighbors(current))
                {
                    int newCost = costSoFar[current]
                        + graph.Cost(current, next);
                    if (!costSoFar.ContainsKey(next)
                        || newCost < costSoFar[next])
                    {
                        if (costSoFar.ContainsKey(next))
                        {
                            costSoFar[next] = newCost;
                        }
                        else
                        {
                            costSoFar.Add(next, newCost);
                        }
                        
                        int priority = newCost + Heuristic(next, goal);
                        frontier.Enqueue(next, priority);
                        if (costSoFar.ContainsKey(next))
                        {
                            cameFrom[next] = current;
                        }
                        else
                        {
                            cameFrom.Add(next, current);
                        }
                        
                    }
                }
            }
        }

        public Stack<Square> GetPath()
        {
            Stack<Square> squares = new Stack<Square>();
            Square current = goal;
            squares.Push(current);
            while (current != start)
            {
                current = cameFrom[current];
                squares.Push(current);
            }

            return squares;
        }
    }
}