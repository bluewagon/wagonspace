﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Eppy;
using Pathfinder;

public class BoardManager : MonoBehaviour
{

    public GameObject Tile1;
    public GameObject Tile2;
    [Tooltip("Empty gameobject to hold the all the GameObjects tiles")] 
    public GameObject Board;

    public int BoardSize = 10;

    private static SquareGrid grid;

	// Use this for initialization
	void Start () 
    {
	    
	}

    void Awake()
    {
        grid = new SquareGrid(BoardSize);
        SetupBoard();
    }
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public static void SetCharacter(int x, int y)
    {
        grid.board[x][y].IsOccupied = true;
    }

    public static bool IsPassable(int x, int y)
    {
        return !grid.board[x][y].IsOccupied && grid.board[x][y].IsPassible;
    }

    public static bool InBounds(int x, int y)
    {
        return grid.InBounds(x, y);
    }

    public static Stack<Square> GetPath(Tuple<int, int> start, Tuple<int, int> goal)
    {
        AStarSearch search = new AStarSearch(grid, grid.board[start.Item1][start.Item2], grid.board[goal.Item1][goal.Item2]);
        return search.GetPath();
    }

    void SetupBoard()
    {
        grid.board.Clear();
        for (int row = 0; row < BoardSize; row++)
        {

            grid.board.Add(new List<Square>());
            for (int col = 0; col < BoardSize; col++)
            {
                if ((col%2 == 0 && row%2 == 0) || col%2 == 1 && row%2 == 1)
                {
                    GameObject tile = Instantiate(Tile1, new Vector3(col, row, 0), Quaternion.identity) as GameObject;
                    tile.transform.parent = Board.transform;
                }
                else
                {
                    GameObject tile = Instantiate(Tile2, new Vector3(col, row, 0), Quaternion.identity) as GameObject;
                    tile.transform.parent = Board.transform;
                }
                grid.board[row].Add(new Square(row, col));
            }
        }
    }
}
