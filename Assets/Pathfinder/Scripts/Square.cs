﻿using System;

namespace Pathfinder
{
    public class Square
    {
        public int x;
        public int y;

        public bool IsPassible; // is this a wall, etc., maybe make it an enum? 0 - GROUND, 1 - WALL, 2 - BUILDING
        public bool IsOccupied; // is there another npc?

        public int Cost; // default is 1

        public Square(int x, int y) : this(x, y, 1, true)
        {
        }

        public Square(int x, int y, int cost) : this(x, y, cost, true)
        {
        }

        public Square(int x, int y, int cost, bool isPassible) : this(x, y, cost, isPassible, false)
        {
        }

        public Square(int x, int y, int cost, bool isPassible, bool isOccupied)
        {
            this.x = x;
            this.y = y;
            this.IsPassible = isPassible;
            this.IsOccupied = isOccupied;
            this.Cost = cost;
        }

        protected bool Equals(Square other)
        {
            return x == other.x && y == other.y;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Square) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = x;
                hashCode = (hashCode*397) ^ y;
                return hashCode;
            }
        }

        public static bool operator ==(Square left, Square right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Square left, Square right)
        {
            return !Equals(left, right);
        }

        public override string ToString()
        {
            return String.Format("({0},{1})", x, y);
        }
    }
}