﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Warp : MonoBehaviour 
{

	public Transform WarpTarget;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	IEnumerator OnTriggerEnter2D(Collider2D other)
	{
		Debug.Log("Entered warp.");
		if (other.CompareTag("Player"))
		{
			yield return StartCoroutine(UIManager.Instance.FadeOut());

			other.gameObject.transform.position = WarpTarget.transform.position;
			Camera.main.transform.position = WarpTarget.transform.position;

			yield return StartCoroutine(UIManager.Instance.FadeIn());
		}
	}
}
