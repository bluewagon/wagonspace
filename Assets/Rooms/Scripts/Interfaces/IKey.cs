﻿using UnityEngine;
using System.Collections;

public interface IKey
{
	void Use();
}
