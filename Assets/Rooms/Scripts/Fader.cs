﻿using UnityEngine;
using System.Collections;

public class Fader : MonoBehaviour 
{

	private bool isFading;
	private Animator anim;
	// Use this for initialization
	void Start () 
	{
		isFading = false;
		anim = GetComponent<Animator>();
	}

	public IEnumerator FadeIn()
	{
		isFading = true;
		anim.SetTrigger("FadeIn");

		while(isFading)
		{
			yield return null;
		}

	}

	public IEnumerator FadeOut()
	{
		Debug.Log("Fading");
		isFading = true;
		anim.SetTrigger("FadeOut");

		while (isFading)
		{
			yield return null;
		}
	}

	public void FadeComplate () 
	{
		Debug.Log("Fade complete");
		isFading = false;
	}
}
