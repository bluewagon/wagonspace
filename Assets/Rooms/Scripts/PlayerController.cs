﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    public float Speed;

    private Rigidbody2D rbody;
    private Animator anim;

	private bool action;

	Vector2 lastMovement = Vector2.zero;

	public float Angle = 10f;
	// Use this for initialization
	void Start ()
	{
	    rbody = GetComponent<Rigidbody2D>();
	    anim = GetComponent<Animator>();
		action = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		Vector2 movement = GameManager.Instance.movement;

	    if (movement != Vector2.zero)
	    {
	        anim.SetBool("iswalking", true);
            anim.SetFloat("input_x", movement.x);
            anim.SetFloat("input_y", movement.y);
			lastMovement = movement;
	    }
	    else
	    {
	        anim.SetBool("iswalking", false);
	    }

        rbody.MovePosition(rbody.position + movement * Time.deltaTime * Speed);

		if (Input.GetButtonDown("Action"))
		{
			action = true;
		}

		if (Input.GetButtonUp("Action"))
		{
			action = false;
		}
	}

	void OnTriggerStay2D(Collider2D other)
	{
		IInteract interact = other.gameObject.GetComponent<IInteract>();
		if (interact != null && action)
		{
			if  ( Vector2.Angle(lastMovement, other.transform.position - transform.position) < Angle) 
			{
				interact.Interact(gameObject);
			}
		}
	}
}
