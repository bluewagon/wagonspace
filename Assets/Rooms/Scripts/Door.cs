﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour, IInteract
{

	public bool IsLocked = false;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void Interact(GameObject caller)
	{
		if (caller.CompareTag("Player"))
		{
			Inventory inventory = caller.gameObject.GetComponent<Inventory>();
			if (inventory.Keys > 0)
			{
				inventory.UseKey();
				IsLocked = false;
			}
		}

		if (!IsLocked)
		{
			Destroy(gameObject);
		}
	}
}
