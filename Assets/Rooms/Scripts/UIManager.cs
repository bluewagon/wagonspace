﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour
{

	public Image Background;

	private Fader fader;

	public static  UIManager Instance = null;
	
	// Use this for initialization
	void Start () 
	{
		fader = Background.GetComponent<Fader>();
		Instance = this;
	}

	public IEnumerator FadeIn()
	{
		GameManager.Instance.Paused = false;
		yield return StartCoroutine(fader.FadeIn());
	}

	public IEnumerator FadeOut()
	{
		GameManager.Instance.Paused = true;
		yield return StartCoroutine(fader.FadeOut());

	}
}
