﻿using UnityEngine;
using System.Collections;

public class Key : MonoBehaviour, IKey, IItem
{

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Player"))
		{
			Inventory inventory = other.gameObject.GetComponent<Inventory>();
			inventory.OnPickup(this);
			Destroy(gameObject);
		}
	}

	public void Use()
	{
		// do nothing
	}

	public void OnPickup()
	{
		Inventory inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
		inventory.OnPickup(this);
	}
}
