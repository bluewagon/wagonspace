﻿using UnityEngine;
using System.Collections;

public class Chest : MonoBehaviour, IInteract 
{

	public GameObject Loot;

	private Animator anim;

	// Use this for initialization
	void Start () 
	{
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void Interact(GameObject caller)
	{
		if (caller.CompareTag("Player"))
		{
			anim.SetTrigger("Interact");
		}
	}

	void OnCompleted()
	{
		IItem item = Loot.GetComponent<IItem>();
		item.OnPickup();
	}
}
