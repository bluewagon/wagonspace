﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour 
{

	public int Keys;
	public Text KeyCount;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void OnPickup(IItem item)
	{
		if (item is IKey)
		{
			Debug.Log("Picked up key");
			AddKey();
		}
	}

	public void AddKey()
	{
		Keys++;
		KeyCount.text = Keys.ToString();
	}

	public void UseKey()
	{
		Keys--;
		KeyCount.text = Keys.ToString();
	}
}
