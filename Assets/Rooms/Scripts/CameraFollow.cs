﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{

    public Transform Target;
	public Canvas UICanvas;
    public float Scale = 4f;
	public float CanvasScale = 0.01f;
    public float CameraFollowSpeed = 0.1f;
    private Camera myCam;

	// Use this for initialization
	void Start ()
	{
	    myCam = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    myCam.orthographicSize = (Screen.height/100f)/Scale;
	    if (Target)
	    {
	        transform.position = Vector3.Lerp(transform.position, Target.transform.position, 0.1f) + new Vector3(0f, 0f, -10f);
	    }
	}
}
