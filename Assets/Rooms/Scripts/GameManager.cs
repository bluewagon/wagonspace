﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour 
{
	public static GameManager Instance = null;

	public Vector2 movement;
	public bool Paused;

	// Use this for initialization
	void Start () 
	{
		Instance = this;

		Paused = false;
		movement = Vector2.zero;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Paused)
		{
			movement = Vector2.zero;
			return;
		}

		float horiz = Input.GetAxisRaw("Horizontal");
		float vert = Input.GetAxisRaw("Vertical");
		movement = new Vector2(horiz, vert);

		if (Input.GetButtonDown("Action"))
		{
			Debug.Log("Action pressed.");
		}
	}
}
